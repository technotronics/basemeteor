// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/configuracao',{
	name: 'configuracaoView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'configuracaoView'
		});
	}
});
