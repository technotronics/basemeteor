export const privateRoutes = FlowRouter.group({
	subscriptions: function() {
		this.register('Configuracao', Meteor.subscribe('Configuracao'));

		this.register('allAlunos', Meteor.subscribe('allAlunos'));
		this.register('allSeries', Meteor.subscribe('allSeries'));
		this.register('allTurmas', Meteor.subscribe('allTurmas'));
		this.register('allPresencas', Meteor.subscribe('allPresencas'));
		this.register('allMatriculas', Meteor.subscribe('allMatriculas'));
		this.register('allPais', Meteor.subscribe('allPais'));
		this.register('allUsuarios', Meteor.subscribe('allUsuarios'));
	},
	prefix: '/admin',
	triggersEnter: [function(context, redirect) {
		// privateRoutes necessitam de autenticacao
		if (!Meteor.userId()) {
			sAlert.error('É preciso se autenticar para acessar este recurso!');
			//redirect('/login'); // Desvia para o Login
			FlowRouter.go('login');
		}
		$('body').addClass('web');
	}]
});

export const publicRoutes = FlowRouter.group({

});
