//Rotas Publicas
import { publicRoutes } from './groups.js'

publicRoutes.route('/', {
	name: 'homeView',
	action: function() {
		BlazeLayout.render('defaultLayout', {
			header: 'headerView',
			main: 'homeView'
		});
	}
});

publicRoutes.route('/login', {
	name: 'loginView',
	action: function() {
		BlazeLayout.render('loginLayout', {
			header: 'headerView',
			main: 'loginView'
		});
	}
});
