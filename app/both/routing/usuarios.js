// Rotas com Autenticação
import { privateRoutes } from './groups.js'

privateRoutes.route('/usuarios',{
	name: 'usuariosView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'usuariosView'
		});
	}
});

privateRoutes.route('/usuarios/form',{
	name: 'usuariosInsertView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'headerView',
			main: 'usuariosFormView'
		});
	}
});
privateRoutes.route('/usuarios/form/:usuario_id',{
	name: 'usuariosUpdateView',
	action: function() {
		BlazeLayout.render('adminLayout', {
			header: 'header',
			main: 'usuariosFormView'
		});
	}
});
